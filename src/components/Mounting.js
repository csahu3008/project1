//mounting lifecycle method means are called when the instance of component are created or inserted
//order of invocation
//constructor(props)

//state getDerivedStateFromProps(props,state)
// When the state of the component depends on changes in props over time
//set the state
//this does not have access to this keyword  not http requests

//render()
//only required method 

//componentDidMount()
// called only once in the lifetime 
// invoked immediately after a componenet and all its children have been rendered to the DOM
//BEst place for side effects ::intereact with DOM or perform any ajax calls


//Unmounting Phase Method
//componentWillUnmount()
//invoked immediatedly before a componenet is unmounted or destroyed 
// used for cancelling network requests,removing event handlers,cancelling any subscriptions
//donto call setState()

//Error Handling METHOD
//static getDerivedStateFromError(error)
//componentDidCatch(error,info)


import LifecycleB from './MountingB'
import React, { Component } from 'react'

class Lifecycle extends Component {
    constructor(props){
        super(props)
        this.state={
            name:'chandra shekhar'
        }
        console.log("Lifecycle A constructor")
    }
    static getDerivedStateFromProps(props,state)
    {  console.log("lifecycle A getDerivedStateFromProps")
        return null
    }
    componentDidMount(){
          console.log("Lifecycle componentDidMount")
    }
    render() {
        console.log("lifecycle A renderd");
        return (
            <div>
                HEllo World
                <LifecycleB />
            </div>
        )
    }

}

export default Lifecycle
