import React from 'react'

function person({person}) {
    return (
        <div>
        <h1>My name is {person.name} </h1>
        <h2>I love {person.skill}</h2>
        <h3>Id - {person.id}</h3>
        </div>
    )
}

export default person
