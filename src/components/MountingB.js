import React, { Component } from 'react'

class LifecycleB extends Component {
    constructor(props){
        super(props)
        this.state={
            name:'chandra shekhar'
        }
        console.log("Lifecycle B constructor")
    }
    static getDerivedStateFromProps(props,state)
    {  console.log("lifecycle B getDerivedStateFromProps")
        return null
    }
    componentDidMount(){
          console.log("Lifecycle B componentDidMount")
    }
    render() {
        console.log("lifecycle B renderd");
        return (
            <div>
                Lifecycle B
            </div>
        )
    }

}

export default LifecycleB
