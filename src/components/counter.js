import React,{ Component } from 'react'
class counter extends Component{
     constructor(){
         super()
         this.state={
             val:0
         }
     }
     changeVal(){
         // this.state.val=this.state.val+1
         //never use  this method directly to assign the value to the state variables they will be reflected in console but not on UI 
         //whenever we wnat to execute the function after the value of state has been changed place it inside callback function in setState()
         
        //  if(this.state.val < 20){
        //  this.setState({
        //      val:this.state.val+1
        //  },()=>console.log(this.state.val)
        //      )
        //     }
        //  else{
        //     this.setState({
        //         val:0
        //     })
        //  }
        this.setState((prevState,props)=>({
            val: prevState.val + parseInt(props.name),
        } ))
         
     }
     incrementFive(){
         this.changeVal();
         this.changeVal();
         this.changeVal();
         this.changeVal();
         this.changeVal();
        //whenever we want to update the previous value in the function we sholud passs the function through setState()
      //react makes  group multiple setState calls into single update for better performance 

     }
    render(){
        return( <div><h1> HElllo Boss </h1>
           <h1 style={{color:'red'}}>{this.state.val}</h1> 
           <button onClick={()=>this.incrementFive() }>Click me to increment</button>
          </div>
        )}
}
export default counter