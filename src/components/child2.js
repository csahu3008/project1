import React from 'react'

function child2(props) {
    return (
        <div>
            <button onClick={()=>{props.sayHEllo("name")}}>Say HEllo</button>
        </div>
    )
}

export default child2
