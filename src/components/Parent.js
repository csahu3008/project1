import React, { Component } from 'react'
import RegularCompo from './RegularCompo'
import PureComponent from './pureComponent'

class Parent extends Component {
    constructor(props) {
        super(props)
        this.state = {
             name:'ram'
        }
    }
    componentDidMount(){
        setInterval(()=>{
            this.setState({
                name:'ram'
            })
        },2000)
    }
    
    render() {
        console.log("parent Component render")
        return (
            <div>
                Parent Component
                <RegularCompo name={this.state.name} />
                <PureComponent name={this.state.name} />
            </div>
        )
    }
}

export default Parent
