import React, { Component } from 'react'
import LifecycleC from './Update2'
class LifecycleB extends Component {
    constructor(props){
        super(props)
        this.state={
            name:'chandra shekhar'
        }
        console.log("Lifecycle B constructor")
    }
    static getDerivedStateFromProps(props,state)
    {  console.log("lifecycle B getDerivedStateFromProps")
        return null
    }
    componentDidMount(){
          console.log("Lifecycle B componentDidMount")
    }
    shouldComponentUpdate(nextprops,nextstate){
        console.log("lifecycle B sholudComponentUpdate")
        return true
    }
    getSnapshotBeforeUpdate(prevprops,prevstates)
    {
        console.log("lifecycle B getSnapShotBeforeUpdate")
        return null
    }
    componentDidUpdate(){
        console.log("Lifecycle B componentDidUpdate")
    }
    changeState=()=>{
        this.setState({
            name:'hero'
        })
    }

    render() {
        console.log("lifecycle B renderd");
        return (
            <div>
                Lifecycle B
                <button onClick={this.changeState}>Change State</button>
                <LifecycleC />
            </div>
        )
    }

}

export default LifecycleB
