import React, { Component } from 'react'

class ChildComponent extends Component {
    render() {
        return (
            <div>
                {/* <button onClick={this.props.greetHandler}>Greet Parent </button> */}
               {/* passing parameters to the parent component using arrow functions */}
                <button onClick={()=>this.props.greetHandler("I am Newest Child")}>Greet Parent </button>
            </div>
        )
    }
}

export default ChildComponent
