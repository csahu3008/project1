//destructuring means unpacking the values from arrays or property from objects into distinct variable
import React from 'react'
// const func2=({name,chars})=>{
//     return(
//         <h1>HEllo {name}  loves {chars}</h1>
//     )
// }
//first way of destrucring props elements
const func2=(props)=>{
    const {name,chars}=props
    return(
        <h1>HEllo {name}  loves {chars}</h1>
    )
}

//second way of destrcturing element in function body
export default func2