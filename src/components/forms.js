import React, { Component } from 'react'

export class forms extends Component {
    constructor(props){
        super(props)
        this.state={
            username:'',
            email:'',
            comment:'',
            course:'',
        }
    }
    handleUsernameChange=(event)=>{
        this.setState({
            username:event.target.value,
        })
    }
    handleEmailChange=(event)=>{
        this.setState({
            email:event.target.value,
        })
    }
    handleCommentChange=(event)=>{
        this.setState({
            comment:event.target.value
        })
    }
    handleSelect=(event)=>{
        this.setState({
            course:event.target.value,
        })
    }
    SubmitForm=(event)=>{
        alert(`${ this.state.username} ${ this.state.email} ${ this.state.comment} ${ this.state.course}`)
     event.preventDefault();
    }
    render() {
        const {username,email,comment,course}=this.state
        return (
            <div>
                <h1>Forms Component</h1>
                <form onSubmit={this.SubmitForm}>
                <div>
                    <label>Username</label>
                    <input type='text' value={ username} onChange={this.handleUsernameChange}  />
                </div>
                <div>
                <label>Email</label>
                <input type='text' value={ email} onChange={this.handleEmailChange}  />
                </div>
                <div>
                <label>Comments</label>
                <textarea value={ comment} onChange={this.handleCommentChange}></textarea>
                </div>
                <div>
                <label>Course</label>
                <select onChange={this.handleSelect} value={ course}>
                    <option value='react'>React</option>
                    <option value='Angular'>Angular</option>
                    <option value='vue'>Vue</option>
                </select>
                </div>
                <button type='submit'>Submit</button>
                </form>
            </div>
        )
    }
}

export default forms
