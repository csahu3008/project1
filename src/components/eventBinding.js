//eventbinding  this keyword works in javascript
import React,{Component } from 'react'
class eventBinding extends Component{
    constructor(props){
        super(props)
        this.state={
            message:"QWERTYUIO"
        }
        //the official and recommended one
        // this.changeContent=this.changeContent.bind(this)

    }
    //normal way of declaration fo function
    // changeContent(){
    //     this.setState({
    //         message:"GOodBYE"
    //     })
   
   //class property as arrow function
   changeContent=()=>{
       this.setState({
           message:"Good Bye"
       })
   }

    // this part will work when we pass the function through onClick event
        //   this.setState(()=>({
    //       message:"HElp EM"
    //    }
    //   ))
    // }
    render(){
        const {name}=this.props
        const {message}=this.state
        return(
            <div>
            <h1>HEllo Friend {name}</h1>
            <h1>{message}</h1>

            {/* event binding method */}
            {/* <button onClick={()=>this.changeContent()}>change Content</button> */}
            {/* <button onClick={this.changeContent.bind(this)}>Click ME</button> */}
            
            <button onClick={this.changeContent}>Click ME</button>

            </div>
        )
    }
}
export default eventBinding