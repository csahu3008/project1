import React from 'react'
import  Person from './person'
//this is for rendering the elements of an array into list
function ListRender() {
    // const L=['ram','karim','kalam','vahida']
    // const List=L.map((ele) => <h1>{ele}</h1>)
    const newData=[
        {
            name:"ram",
            skill:"django",
            id:"1"
        },
        {
            name:"ramiu",
            skill:"react",
            id:"2"  
        },
        {
            name:"Ketan",
            skill:"angular",
            id:"3"
        },
        {
            name:"kiosh",
            skill:"angular",
            id:"4"
        }
    ]
    // const complete=newData.map((person)=>(   <p><h1>My name is {person.name} </h1>
    //     <h2>I love {person.skill}</h2>
    //     <h3>Id - {person.id}</h3>
    //     </p>))

      //                    KEY 
                 //a key is a speacial attribute you need to include when creating lists of elements.
                //keys give the elements a stable identity.
            //keys help react identify which items have changed,are added or are removed .
   
       //when to use inde as key
       //@ The items in list donot have unique id
       //@ the list is a static list and will not change
       //@ the list will never be reordered or filtered
   
            const personList=newData.map((person,index) => (<Person key={person.id} person={person} />))
    return (
        <div> 
       {/* {  L.map(name => <h2 style={{color:'green'}}><li> {name}</li> </h2>) }
             <h1>{names[0]}</h1>
            <h1>{names[1]}</h1>
            <h1>{names[2]}</h1>
            <h1>{names[3]}</h1> 
            names.map(name => <h2> {name} </h2>) */}
          {/* {List} */}
          {/* {complete} */}
           {personList}
          </div>
    )
}
export default ListRender