import React,{Component} from 'react'
class UserGreet extends Component{
    constructor(props){
        super(props)
        this.state={
            isLoggedIn:true
        }
    }
    render(){
        // if(this.state.isLoggedIn)
        // {
        //     return(
        //         <div>
        //         <h1>Hello Chandra shekhar</h1>
        //         </div>
        //     )   
        // }true
        // else{
        //     return(
        //         <div>
        //         <h1>Hello Guest</h1>
        //         </div>
        //     )  
        // }
        // let message
        // if(this.state.isLoggedIn)
        // {
        //    message=<div>Hello Chandra shekhar</div>
        // }
        // else{
        //   message=<div>Hello Guest</div>
        // }
        // return <div>{message}</div>
        return(
            //ternary operation
            // this.state.isLoggedIn?
            // <h1>Hello Chandra shekhar</h1>:
            // <h1>Hello Guest</h1>

            //short circuit method
            this.state.isLoggedIn && (<h1>Hello Chandra shekhar</h1>)
        )
    }
}
export default UserGreet