//A pure component implements shouldUpdate() with a swallow prop and state comparision
import React, { PureComponent } from 'react'

class pureComponent extends PureComponent {
    render() {
        console.log("Pure Component render")
        return (
            <div>
                Pure Component
                {this.props.name}
            </div>
        )
    }
}

export default pureComponent
