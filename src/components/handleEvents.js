//this for demonstration of event handler events 
// for functional components 
//never use {clickHandler()} because this is function call, not a function 
import React from 'react'
const HandleEvent=()=>{
    function ClickHandler(){
        console.log("Button was clicked");
    }
    return(
        <div>
           <button onClick={ClickHandler}>Click mE</button>
        </div>
    )
}
export default HandleEvent