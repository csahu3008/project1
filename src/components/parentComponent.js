import React,{Component} from 'react'
import Child2 from './child2'
import ChildComponent from './ChildComponent'
//passing the methods as props to the child Component
class ParentCompo extends Component{
    constructor(){
        super()
        this.state={
            parentName:'Parent'
        }
        this.greetParent=this.greetParent.bind(this)
    }
    greetParent(name){
        alert("hello "+this.state.parentName + name)
    }
    sayHello=(child)=>{ alert( "Say HEllo Method Invoked by "+child) }
    render(){
        return (
            <div>
            <ChildComponent greetHandler={this.greetParent} />
            <Child2 sayHEllo={this.sayHello} />
            </div>
        )
    }
}
export default ParentCompo