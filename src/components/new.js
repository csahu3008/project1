import React ,{Component } from 'react'
class New extends Component{
    constructor(){
        super()
        this.state={
            message:'Welcome visitor'
        }
    }
    changeMessage(){
        this.setState(
            {
                message:'Thank U For Subscribing',
                child:'children node'
            }
        )
    }
    revertMessage(){
        this.setState({
            message:'You have clicked it twice',
            child:"changed"
        })
    }
    getUser(){
        this.setState({
            user: this.props.name
        })
    }
    render(){
        return (
            <div>
            <h1>{this.state.message} </h1>
            <button onClick={ ()=>{ this.revertMessage() ;this.changeMessage() } }> Subscribe</button>
            {this.props.children}
            <button onClick={()=>this.getUser()}>GEt USername</button>
            {this.state.child}
            <h1>You must be {this.state.user}</h1>
            </div>
        )
    }
}
export default New