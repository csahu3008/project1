import React, { Component } from 'react'
class LifecycleC extends Component {
    constructor(props){
        super(props)
        this.state={
            name:'chandra shekhar'
        }
        console.log("Lifecycle C constructor")
    }
    static getDerivedStateFromProps(props,state)
    {  console.log("lifecycle C getDerivedStateFromProps")
        return null
    }
    componentDidMount(){
          console.log("Lifecycle C componentDidMount")
    }
    shouldComponentUpdate(){
        console.log("lifecycle C sholudComponentUpdate")
        return true
    }
    getSnapshotBeforeUpdate(prevprops,prevstates)
    {
        console.log("lifecycle C getSnapShotBeforeUpdate")
        return null
    }
    componentDidUpdate(){
        console.log("Lifecycle C componentDidUpdate")
    }
    render() {
        console.log("lifecycle C renderd");
        return (
            <div>
                Lifecycle C
            
            </div>
        )
    }

}

export default LifecycleC
