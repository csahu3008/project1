import React from 'react'
import './style.css'
function Stylesheet(props) {
     let  className=props.primary?'primary':'secondary'
    return (
        <div>
            <h1 className={`${className} font-increase`}>Stylesheet</h1>
        </div>
    )
}

export default Stylesheet
