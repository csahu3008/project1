import React from 'react';
// import logo from './logo.svg';
//javascript XML (JSX)
import './App.css';
import Parent from './components/Parent'
// import Table from './components/table'
// import Fragment from './components/Fragment'
// import Forms from './components/forms'
// import LifeCycleB from './components/Update'
// import LifeCycle from './components/Mounting'
// import LifecycleB from './components/Update.js';
// import   Stylesheet from './components/Stylesheet'
// import Inline from './components/inline'
// import ListRender from './components/listender'
// import UserGreet from './components/usergreeting'
// import New from './components/new'
// // import  Greet from './components/Greet'
// // import Welcome from './components/Welcome'
// // import Hello from './components/Hello'
// // import Without from './components/Without'
// import Counter from './components/counter'
// import Func2 from './components/new2'
// import HandleEvent from './components/handleEvents'
// import HandleEventsClass from './components/classEvents'
// import EventBinding from './components/eventBinding'
// import ParentCompo from './components/parentComponent'
// import './components/style.css'
// import styles from './components/style.module.css'
function App() {
  return (
    <div className="App">
    <Parent />

    {/* <Table /> */}
    {/* <Fragment /> */}
    {/* <Forms />
    <LifeCycleB /> */}
    {/* <LifeCycle name='verma' /> */}
    {/* <Stylesheet className='primary' />
    <h1 className='error'>error</h1>
    <h1 className={styles.success}>Sucesss</h1>
    <Inline  ></Inline> */}
    {/* <ListRender /> */}
    {/* <UserGreet /> */}
      {/* <Greet name='ravi' >
        <p>This is Props Children</p>
      </Greet>
      <Greet name='John'>
      <input type='button' value='BUTTON'/>
      </Greet>
      <Greet name='Joid' />
      <Welcome name='kk' />
      <Welcome name='wonders' />
      <Welcome name='ERTYU' >
      <button>THIS IS Children oF CLass components</button>
      </Welcome>
      <Hello />
      <Without /> */}
      {/* <New name='Roberts Downy'>
      <h2>Child Element</h2>
      </New>
      <Counter name='2' />
      <Func2 name='hatay' chars='rubin' /> */}
      {/* <HandleEvent /> */}
    {/* <HandleEventsClass name='Chandra shekhar' message='Message sent by me' /> */}
    {/* <EventBinding name='Chandra shekhar' />

    <ParentCompo /> */}
    </div>
  );
}

export default App;
